package shopping;

import java.util.*;

public class Shop 
{
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		Customer c=new Customer();
		Product p=new Product();
		Cart ca=new Cart();
		int ch;
		while(true)
		{
			System.out.println("1:Shopping\n2:Cart\n3:Exit\nEnter your choice");
			ch=sc.nextInt();
			switch(ch)
			{
				case 1: System.out.println("1:Mobile\n2:Laptop\nEnter name of the product");
						p.name=sc.next();
						
						if(p.name.equalsIgnoreCase("Mobile"))
						{
							System.out.println("Brand\tPrice\t\tQuantity");
							Iterator itM=p.Mobile.entrySet().iterator();
							while(itM.hasNext())
							{
								Map.Entry<String,List<Double>> e=(Map.Entry<String,List<Double>>)itM.next();
								System.out.println(e.getKey()+"\t"+e.getValue().get(0)+"\t\t"+e.getValue().get(1));
							}
							System.out.println("Enter brand of the product");
							p.brand=sc.next();
							System.out.println("Enter quantity of the product");
							p.qty=sc.nextDouble();
							if(p.qty<=p.Mobile.get(p.brand).get(1))
							{
								
								if(p.Mobile.containsKey(p.brand))
								{
									p.price=p.Mobile.get(p.brand).get(0);
								}
								System.out.println("Do you want to add in cart");
								String choice=sc.next();
								if(choice.equalsIgnoreCase("Yes"))
								{
									ca.prod.put(p.brand, Arrays.asList(p.price,p.qty,(p.price*p.qty)));
									Double x=p.Mobile.get(p.brand).get(1);
									p.Mobile.get(p.brand).set(1,x-p.qty);
									System.out.println("Your items are added into cart");
								}
							}
							else
							{
								System.out.println("Your required quantity is not in stock");
							}
						}
						else if(p.name.equalsIgnoreCase("Laptop"))
						{
							System.out.println("Brand\tPrice\t\tQuantity");
							Iterator itL=p.Laptop.entrySet().iterator();
							while(itL.hasNext())
							{
								Map.Entry<String,List<Double>> l=(Map.Entry<String,List<Double>>)itL.next();
								System.out.println(l.getKey()+"\t"+l.getValue().get(0)+"\t"+l.getValue().get(1));
							}
							System.out.println("Enter brand of the product");
							p.brand=sc.next();
							System.out.println("Enter quantity of the product");
							p.qty=sc.nextDouble();
							if(p.qty<=p.Laptop.get(p.brand).get(1))
							{
								if(p.Laptop.containsKey(p.brand))
								{
									p.price=p.Laptop.get(p.brand).get(0);
								}
								System.out.println("Do you want to add in cart");
								String choice=sc.next();
								if(choice.equalsIgnoreCase("Yes"))
								{
									ca.prod.put(p.brand, Arrays.asList(p.price,p.qty,(p.price*p.qty)));
									System.out.println("Your items are added into cart");
									Double x=p.Laptop.get(p.brand).get(1);
									p.Laptop.get(p.brand).set(1,x-p.qty);
								}
							}
							else
							{
								System.out.println("Your required quantity is not in stock");
							}
						}
						else
						{
							System.out.println("Invalid Choice");
						}
						break;
		        case 2: ca.displayInvoice();
				        break;
                case 3: sc.close();
				        System.exit(ch);
				        break;
                default: System.out.println("Invalid Choice");
			}
			
		}
	}
}
