package shopping;

import java.util.*;

public class Cart
{
	int id;
	HashMap<String,List<Double>>prod=new HashMap<String,List<Double>>();
	double total=0;
	public void displayInvoice()
	{
		Iterator itP=prod.entrySet().iterator();
		System.out.println("------------------Cart---------------------------");
		System.out.println("Brand\tPrice\t\tQuantity\tSub-total");
		while(itP.hasNext())
		{
			Map.Entry<String, List<Double>>pm=(Map.Entry<String, List<Double>>)itP.next();
			System.out.println(pm.getKey()+"\t"+pm.getValue().get(0)+"\t\t"+pm.getValue().get(1)+"\t\t"+pm.getValue().get(2));
			total+=pm.getValue().get(2);
		}
		System.out.println("\nTotal Amount:"+total+"\n");
	}
}
