package shopping;

import java.util.*;
public class Customer 
{
	String name,email;
	long phone;
	Scanner sc=new Scanner(System.in);
	public Customer()
	{
		System.out.println("Enter Your name ");
		name=sc.next();
		System.out.println("Enter Your Email ");
		email=sc.next();
		System.out.println("Enter Your Phone Number");
		phone=sc.nextLong();
	}
	public void CustomerDetails(String name, String email, long phone) 
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
	}
	
	public void displayDetails()
	{
		System.out.println("Name: "+name);
		System.out.println("Email: "+email);
		System.out.println("Phone Number: "+phone);
	}
	
	
}
